# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field


class StoresItem(scrapy.Item):
    # define the fields for your item here like:
    title = Field()
    description = Field()
    stars = Field()
    author_name = Field()
    date = Field()
    url = Field()
    product_id = Field()
    spider = Field()
    badges = Field()
    comments_count = Field()
    helpful = Field()