# -*- coding: utf-8 -*-
import scrapy
import datetime
import json
import re

from stores.items import StoresItem
from scrapy_splash import SplashRequest


class TargetSpider(scrapy.Spider):
    name = 'target'
    allowed_domains = ['target.com']
    start_urls = ['https://www.target.com/p/lg-65-4k-uhd-smart-tv-65uk6090pua/-/A-53871914']

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url, self.parse,
                endpoint='/render.html',
                args={'wait': .5,
                  'js_source': 'document.body',
                  'proxy': 'http://112.314.11.74:74369'
                  })   

    def parse(self, response):
        #from scrapy.shell import inspect_response
        #inspect_response(response, self)

        #clean the returned script and create an object of it using this regex 
        try:
            script = response.xpath("//*[contains(.,'window.__PRELOADED_STATE__')]").extract_first()#response.xpath('//script').extract(
            regex = r"window.__PRELOADED_STATE__= (.+?)</script>"
            matches = re.search(regex, str(script))
            data_obj = json.loads(matches.group(1))
            item['comments_count'] = data_obj['product']['productDetails']['ratingAndReview']['coreStats']['RatingReviewTotal']
            item['stars'] = data_obj['product']['productDetails']['ratingAndReview']['coreStats']['AverageOverallRating']
        except:
            pass

        item = StoresItem()
        title = response.xpath('//title/text()').extract_first().split(':')
        item['title'] = title[0]
        item['description'] = response.xpath('//*[@name="description"]/@content').extract_first()
        item['author_name'] = 'no available'
        item['url'] = response.url
        item['product_id'] = 'pending'
        item['spider'] = self.name
        item['badges'] = 'no available'
        now = datetime.datetime.now()
        item['date'] = now.strftime("%Y-%m-%d %H:%M")


        comments = '?showOnlyReview=true'
        item['helpful'] = ''#pending
        #absolute_next_page_url = response.urljoin(comments)
        absolute_next_page_url = response.url + comments
        print(absolute_next_page_url)

        from scrapy.http.request import Request
        url = 'https://www.target.com/p/lg-65-4k-uhd-smart-tv-65uk6090pua/-/A-53871914?showOnlyReview=true'
        req = Request(url=url)
        #fetch(req)
        yield scrapy.Request(absolute_next_page_url)
        #yield item



# import json
# import re

# script = response.xpath('//script').extract()
# script = script[5]
# regex = r"window.__PRELOADED_STATE__= (.+?)</script>"
# matches = re.search(regex, str(script))
# data_obj = json.loads(matches.group(1))




        #clean the returned script and create an object of it using this regex 
        #regex = r"<script class=\"notranslate\">window.__PRELOADED_STATE__= (.+?)</script>"
        #regex = r"TotalReviewCount\":(.+?),"
        #matches = re.search(regex, str(response.body))
        #regex = r"AverageOverallRating\":(.+?),"
        #stars = re.search(regex, str(response.body))
        #matches = re.search(regex, str(a))
        #data_obj = json.loads(matches.group(1))

# from scrapy.http.request import Request
# url = 'https://www.target.com/p/lg-65-4k-uhd-smart-tv-65uk6090pua/-/A-53871914?showOnlyReview=true'
# Request(url=url)
# fetch(req)